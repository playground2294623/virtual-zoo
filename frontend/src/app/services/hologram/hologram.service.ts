import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HologramService {
  api = environment.apiBase;
  constructor(private http:HttpClient) { }


  getHolograms() {
    return this.http.get(`${this.api}holograms/`);
  }

  getHologram(id:number) {
    return this.http.get(`${this.api}holograms/${id}/`);
  }

  addHologram(hologram:any) {
    return this.http.post(`${this.api}holograms/`, hologram);
  }

  updateHologram(id:number, hologram:any) {
    return this.http.put(`${this.api}holograms/${id}/`, hologram);
  }

  deleteHologram(id:number) {
    return this.http.delete(`${this.api}holograms/${id}/`);
  }

  deleteHolograms() {
    return this.http.delete(`${this.api}holograms/all`);
  }
}
