import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class CustomToastrService {

  constructor(private toastr: ToastrService) { }

  error(error:HttpErrorResponse, title:string) {
    this.toastr.error(
      title +': <br><br>' + 
      error.message + '<br><br>' + 
      JSON.stringify(error.error),'', { enableHtml: true }
    );
  }

  success(message:string) {
    this.toastr.success(message);
  }

}
