import { Component } from '@angular/core';
import { ChatService } from '../../services/chat/chat.service';
import { FormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-chat',
  standalone: true,
  imports: [FormsModule, NgFor, NgIf, TranslateModule],
  templateUrl: './chat.component.html',
  styleUrl: './chat.component.scss',
})
export class ChatComponent {
  messages: string[] = ['Bitte zweites Fenster Öffnen'];
  newMessage: string = '';
  isMinimized: boolean = true;

  constructor(private chatService: ChatService) {}

  ngOnInit(): void {
    this.chatService.getMessages().subscribe((message: any) => {
      this.messages.push(message.message);
      if (this.messages.length > 6) {this.messages.shift();}
    });
  }

  sendMessage(): void {
    if (this.newMessage.trim()) {
      this.chatService.sendMessage(this.newMessage);
      this.newMessage = '';
    }
  }

  toggleMinimize(): void {
    this.isMinimized = !this.isMinimized;
  }
}
