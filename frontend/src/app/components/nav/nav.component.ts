import { NgFor } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule, TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-nav',
  standalone: true,
  imports: [RouterModule, TranslateModule, NgFor],
  templateUrl: './nav.component.html',
  styleUrl: './nav.component.scss'
})

export class NavComponent implements OnInit {
  languages = ['de', 'en'];
  constructor(public translateService: TranslateService) {
  }

  ngOnInit() {
    this.translateService.addLangs(this.languages);    
    const storageLang = localStorage.getItem('language') 
    if(storageLang) this.translateService.use(storageLang);
    if(!storageLang) this.translateService.use(this.languages[0]);
  }

  languageChangeHandler(language: string) {
    this.translateService.use(language);
    localStorage.setItem('language', language);
  }

}
