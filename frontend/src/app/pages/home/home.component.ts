import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgFor } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [NgFor],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit{
  constructor(private router:Router) { }

  cards = Array.from({ length: 49 }, (_, i) => ({
    id: i + 1,
    title: `Card Title ${i + 1}`,
    text: 'Some quick example text to build on the card title and make up the bulk of the card\'s content.'
  }));

  ngOnInit() {
  }

  selectItem(id:Number){
    // this.router.navigate(['']);
  }

}
