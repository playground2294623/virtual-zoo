import { HologramService } from './../../../services/hologram/hologram.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomToastrService } from '../../../services/toastr/custom-toastr.service';

@Component({
  selector: 'app-hologram-detail',
  standalone: true,
  imports: [TranslateModule, FormsModule],
  templateUrl: './hologram-detail.component.html',
  styleUrl: './hologram-detail.component.scss',
})
export class HologramDetailComponent implements OnInit{
  hologram: any = {
    id: '',
    name: '',
    weight: null ,
    superpower: '',
    extinct_since: null ,
    trivia: '',
  };
  constructor(
    private route: ActivatedRoute,
    private hologramService: HologramService,
    protected location: Location,
    private toastr: CustomToastrService
  ) {}

  ngOnInit() {
    const id: any = this.route.snapshot.paramMap.get('id');
    if (id === 'new') {
    }
    if (Number(id)) {
      this.hologramService.getHologram(id).subscribe((hologram:any) => {
        this.hologram = hologram;
      });
    }
  }

  submitHologram() {
    if (this.hologram.id) {
      this.hologramService
        .updateHologram(+this.hologram.id, this.hologram)
        .subscribe({
          next: () => {
            this.toastr.success('Hologram updated successfully');
            this.location.back();
          },
          error: (error: HttpErrorResponse) => {
            this.toastr.error(error, 'Error updating hologram');
          },
        });
    }    
    if (!this.hologram.id) {
      this.hologramService.addHologram(this.hologram).subscribe({
        next: () => {
          this.toastr.success('Hologram added successfully');
          this.location.back();
        },
        error: (error:HttpErrorResponse) => {
          console.log(error);
          this.toastr.error(error, 'Error adding hologram');
        },
      });
    }
  }

  deleteHologram() {
    this.hologramService.deleteHologram(+this.hologram.id).subscribe({
      next: () => {
        this.toastr.success('Hologram deleted successfully');
        this.location.back();
      },
      error: (error: HttpErrorResponse) => {
        this.toastr.error(error, 'Error deleting hologram');
      },
    });
  }
}
