import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HologramDetailComponent } from './hologram-detail.component';

describe('HologramDetailComponent', () => {
  let component: HologramDetailComponent;
  let fixture: ComponentFixture<HologramDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HologramDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HologramDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
