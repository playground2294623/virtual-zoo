import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { HOLOGRAMS } from '../../mocks/HOLOGRAMS';
import { NgFor, NgIf, SlicePipe } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HologramService } from '../../services/hologram/hologram.service';
import { Router, RouterModule } from '@angular/router';
import { concat } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CustomToastrService } from '../../services/toastr/custom-toastr.service';
import { HttpErrorResponse } from '@angular/common/http';
// import { TranslateMoodule } from '@ngx-translate/core/public-api';

@Component({
  selector: 'app-holograms',
  standalone: true,
  imports: [
    MatTableModule,
    MatSortModule,
    MatSort,
    SlicePipe,
    NgFor,
    NgIf,
    TranslateModule,
    RouterModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  templateUrl: './holograms.component.html',
  styleUrl: './holograms.component.scss',
})
export class HologramsComponent implements OnInit {
  dataSource = new MatTableDataSource([]);
  displayedColumns = [
    'id',
    'name',
    'weight',
    'superpower',
    'extinct_since',
    'trivia',
    'action',
  ];

  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  // @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private hologramService: HologramService,
    private router: Router,
    private toastr: CustomToastrService
  ) {}

  async ngOnInit() {
    this.getHolograms();
  }

  search(searchString: String = '') {
    this.dataSource.filter = searchString.trim().toLowerCase();
  }

  applyFilter(event: Event) {
    this.search((event.target as HTMLInputElement).value);
  }

  getHolograms() {
    this.hologramService.getHolograms().subscribe({
      next: (holograms: any) => {
        this.dataSource = new MatTableDataSource(holograms);
        this.dataSource.sort = this.sort;
      },
      error: (error: HttpErrorResponse) => {
        this.toastr.error(error, 'Error getting holograms');
      },
    });
  }

  async deleteHologram(hologramId: number) {    
    this.hologramService.deleteHologram(hologramId).subscribe({
      next: () => {
        this.toastr.success('Hologram deleted successfully');
        this.getHolograms();
      },
      error: (error: HttpErrorResponse) => {
        this.toastr.error(error, 'Error deleting hologram');
      },
    });
  }

  async resetTable() {
    concat(
      this.hologramService.deleteHolograms(),
      this.hologramService.addHologram(HOLOGRAMS)
    ).subscribe({
      complete: () => {
        this.toastr.success('Table reset successfully');
      },
      error: (error: HttpErrorResponse) => {
        this.toastr.error(error, 'Error resetting table');
      }, next: (data:any) => {
        console.log(data);
      }
    }).add(() => {
      this.getHolograms();
    });    
  }

  editHologram(hologramId: number) {
    this.router.navigate([`/holograms/${hologramId}`]);
  }
}
