import {
  HttpHandlerFn,
  HttpInterceptorFn,
  HttpRequest,
} from '@angular/common/http';

import { catchError, throwError } from 'rxjs';

export const AppInterceptor: HttpInterceptorFn = (
  req: HttpRequest<any>,
  next: HttpHandlerFn
) => {
  // const token = localStorage.getItem('token');
  // Handle requests

  return next(req)
    .pipe(
    catchError((error) => {
      // Handle errors
      return throwError(() => error);
    })
  );
};
