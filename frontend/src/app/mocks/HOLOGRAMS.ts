export const HOLOGRAMS = [
  {
    "id": 1,
    "name": "Dodo",
    "weight": "20",
    "superpower": "Flightless bird",
    "extinct_since": "1662",
    "trivia": "The Dodo was native to Mauritius and went extinct due to human activities and introduced species."
  },
  {
    "id": 2,
    "name": "Megalodon",
    "weight": "60000",
    "superpower": "Giant shark",
    "extinct_since": "-2600000",
    "trivia": "The Megalodon was one of the largest predators ever to have lived."
  },
  {
    "id": 3,
    "name": "Tasmanian Tiger",
    "weight": "30",
    "superpower": "Carnivorous marsupial",
    "extinct_since": "1936",
    "trivia": "Also known as the Thylacine, it was the largest carnivorous marsupial of modern times."
  },
  {
    "id": 4,
    "name": "Passenger Pigeon",
    "weight": "0.5",
    "superpower": "Massive flocks",
    "extinct_since": "1914",
    "trivia": "Passenger pigeons once formed massive flocks, some estimated at billions of birds."
  },
  {
    "id": 5,
    "name": "Woolly Mammoth",
    "weight": "6000",
    "superpower": "Cold resistance",
    "extinct_since": "-2000",
    "trivia": "Woolly Mammoths roamed the northern parts of North America, Europe, and Asia."
  },
  {
    "id": 6,
    "name": "Saber-toothed Tiger",
    "weight": "300",
    "superpower": "Massive canines",
    "extinct_since": "-8000",
    "trivia": "Known for their large, curved saber-shaped canine teeth."
  },
  {
    "id": 7,
    "name": "Great Auk",
    "weight": "5",
    "superpower": "Strong swimmer",
    "extinct_since": "1844",
    "trivia": "The Great Auk was a flightless bird that was hunted to extinction for its feathers."
  },
  {
    "id": 8,
    "name": "Steller's Sea Cow",
    "weight": "10000",
    "superpower": "Large herbivore",
    "extinct_since": "1768",
    "trivia": "Steller's Sea Cow was discovered by Europeans in 1741 and hunted to extinction within 27 years."
  },
  {
    "id": 9,
    "name": "Quagga",
    "weight": "300",
    "superpower": "Half-striped",
    "extinct_since": "1883",
    "trivia": "The Quagga was a subspecies of the plains zebra that had distinctive striping only on the front half of its body."
  },
  {
    "id": 10,
    "name": "Irish Elk",
    "weight": "700",
    "superpower": "Gigantic antlers",
    "extinct_since": "-5700",
    "trivia": "The Irish Elk had the largest antlers of any known deer species."
  },
  {
    "id": 11,
    "name": "Moa",
    "weight": "230",
    "superpower": "Flightless bird",
    "extinct_since": "1440",
    "trivia": "Moa were large flightless birds native to New Zealand."
  },
  {
    "id": 12,
    "name": "Pyrenean Ibex",
    "weight": "80",
    "superpower": "Agile climber",
    "extinct_since": "2000",
    "trivia": "The Pyrenean Ibex was one of the first animals to be brought back from extinction through cloning, though the clone lived only briefly."
  },
  {
    "id": 13,
    "name": "Caribbean Monk Seal",
    "weight": "200",
    "superpower": "Marine mammal",
    "extinct_since": "1952",
    "trivia": "The Caribbean Monk Seal was declared extinct due to overhunting by humans."
  },
  {
    "id": 14,
    "name": "Aurochs",
    "weight": "1000",
    "superpower": "Wild cattle",
    "extinct_since": "1627",
    "trivia": "Aurochs were the wild ancestors of domestic cattle."
  },
  {
    "id": 15,
    "name": "Heath Hen",
    "weight": "0.9",
    "superpower": "Ground nesting",
    "extinct_since": "1932",
    "trivia": "The Heath Hen was once found in the heathlands of New England."
  },
  {
    "id": 16,
    "name": "Thylacine",
    "weight": "30",
    "superpower": "Marsupial predator",
    "extinct_since": "1936",
    "trivia": "The Thylacine was the largest known carnivorous marsupial of modern times."
  },
  {
    "id": 17,
    "name": "Haast's Eagle",
    "weight": "15",
    "superpower": "Giant raptor",
    "extinct_since": "1400",
    "trivia": "Haast's Eagle was the largest eagle known to have existed, native to New Zealand."
  },
  {
    "id": 18,
    "name": "Golden Toad",
    "weight": "0.05",
    "superpower": "Bright coloration",
    "extinct_since": "1989",
    "trivia": "The Golden Toad was known for its vivid golden-orange skin."
  },
  {
    "id": 19,
    "name": "Baiji",
    "weight": "160",
    "superpower": "Freshwater dolphin",
    "extinct_since": "2006",
    "trivia": "The Baiji was a freshwater dolphin species native to the Yangtze River in China."
  },
  {
    "id": 20,
    "name": "Ivory-billed Woodpecker",
    "weight": "0.57",
    "superpower": "Large woodpecker",
    "extinct_since": "1944",
    "trivia": "The Ivory-billed Woodpecker was one of the largest woodpeckers in the world."
  },
  {
    "id": 21,
    "name": "Western Black Rhinoceros",
    "weight": "850",
    "superpower": "Thick skin",
    "extinct_since": "2011",
    "trivia": "The Western Black Rhinoceros was declared extinct due to poaching."
  },
  {
    "id": 22,
    "name": "Bubal Hartebeest",
    "weight": "200",
    "superpower": "Swift runner",
    "extinct_since": "1923",
    "trivia": "The Bubal Hartebeest was a species of antelope that lived in North Africa."
  },
  {
    "id": 23,
    "name": "Japanese Sea Lion",
    "weight": "450",
    "superpower": "Marine mammal",
    "extinct_since": "1970",
    "trivia": "The Japanese Sea Lion was once found in the coastal waters of Japan."
  },
  {
    "id": 24,
    "name": "Bluebuck",
    "weight": "160",
    "superpower": "Grazing antelope",
    "extinct_since": "1800",
    "trivia": "The Bluebuck was a species of antelope that lived in South Africa."
  },
  {
    "id": 25,
    "name": "Tecopa Pupfish",
    "weight": "0.01",
    "superpower": "Thermal tolerance",
    "extinct_since": "1981",
    "trivia": "The Tecopa Pupfish was native to the hot springs of the Mojave Desert."
  },
  {
    "id": 26,
    "name": "Pinta Island Tortoise",
    "weight": "250",
    "superpower": "Longevity",
    "extinct_since": "2012",
    "trivia": "Lonesome George was the last known individual of the Pinta Island Tortoise."
  },
  {
    "id": 27,
    "name": "Falkland Island Wolf",
    "weight": "14",
    "superpower": "Predatory",
    "extinct_since": "1876",
    "trivia": "The Falkland Island Wolf was the only native land mammal of the Falkland Islands."
  },
  {
    "id": 28,
    "name": "South Island Kōkako",
    "weight": "0.23",
    "superpower": "Melodic call",
    "extinct_since": "2007",
    "trivia": "The South Island Kōkako was known for its hauntingly beautiful song."
  },
  {
    "id": 29,
    "name": "Laysan Rail",
    "weight": "0.15",
    "superpower": "Flightless bird",
    "extinct_since": "1944",
    "trivia": "The Laysan Rail was a small, flightless bird native to Hawaii."
  },
  {
    "id": 30,
    "name": "Caspian Tiger",
    "weight": "240",
    "superpower": "Stealth hunter",
    "extinct_since": "1970",
    "trivia": "The Caspian Tiger was one of the largest tiger subspecies."
  },
  {
    "id": 31,
    "name": "Dusky Seaside Sparrow",
    "weight": "0.02",
    "superpower": "Insect eater",
    "extinct_since": "1987",
    "trivia": "The Dusky Seaside Sparrow was native to the marshes of Florida."
  },
  {
    "id": 32,
    "name": "Javan Tiger",
    "weight": "140",
    "superpower": "Stealth hunter",
    "extinct_since": "1970",
    "trivia": "The Javan Tiger was native to the Indonesian island of Java."
  },
  {
    "id": 33,
    "name": "Bali Tiger",
    "weight": "100",
    "superpower": "Stealth hunter",
    "extinct_since": "1950",
    "trivia": "The Bali Tiger was the smallest tiger subspecies."
  },
  {
    "id": 34,
    "name": "Spix's Macaw",
    "weight": "0.3",
    "superpower": "Bright plumage",
    "extinct_since": "2000",
    "trivia": "Spix's Macaw is critically endangered and extinct in the wild."
  },
  {
    "id": 35,
    "name": "Guam Kingfisher",
    "weight": "0.06",
    "superpower": "Bright plumage",
    "extinct_since": "1986",
    "trivia": "The Guam Kingfisher is extinct in the wild but survives in captivity."
  },
  {
    "id": 36,
    "name": "Kauaʻi ʻōʻō",
    "weight": "0.03",
    "superpower": "Melodic song",
    "extinct_since": "1987",
    "trivia": "The Kauaʻi ʻōʻō was a small Hawaiian forest bird with a beautiful song."
  },
  {
    "id": 37,
    "name": "New Zealand Grayling",
    "weight": "0.5",
    "superpower": "Agile swimmer",
    "extinct_since": "1930",
    "trivia": "The New Zealand Grayling was a freshwater fish native to New Zealand."
  },
  {
    "id": 38,
    "name": "Norfolk Island Kākā",
    "weight": "0.45",
    "superpower": "Bright plumage",
    "extinct_since": "1851",
    "trivia": "The Norfolk Island Kākā was a brightly colored parrot native to Norfolk Island."
  },
  {
    "id": 39,
    "name": "Atlas Bear",
    "weight": "450",
    "superpower": "Large size",
    "extinct_since": "1870",
    "trivia": "The Atlas Bear was the only bear native to Africa."
  },
  {
    "id": 40,
    "name": "Bubalina Hartebeest",
    "weight": "200",
    "superpower": "Grazing antelope",
    "extinct_since": "1923",
    "trivia": "The Bubalina Hartebeest was once found in the savannas of North Africa."
  }
]