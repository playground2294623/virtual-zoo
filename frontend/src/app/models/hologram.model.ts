
export interface Hologram {
  id: string;
  name: string;
  weight: number;
  superpower: string;
  extinct_since: Date;
  trivia: string;
}