import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { HologramsComponent } from './pages/holograms/holograms.component';
import { ImpressumComponent } from './pages/impressum/impressum.component';
import { HologramDetailComponent } from './pages/holograms/hologram-detail/hologram-detail.component';


export const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'holograms', component: HologramsComponent},
  {path: 'holograms/:id', component: HologramDetailComponent},
  {path: 'impressum', component: ImpressumComponent},
  {path: '**', redirectTo: 'home', pathMatch: 'full'}
];
