from django.db import models

# Create your models here.
class Hologram(models.Model):
    name = models.CharField( null=False, blank=False, max_length=63)
    weight = models.FloatField(null=True, blank=True)
    superpower = models.CharField(max_length=63,null=True, blank=True)
    extinct_since = models.IntegerField(null=True, blank=True)
    trivia = models.TextField(default="", max_length=255, null=True, blank=True)