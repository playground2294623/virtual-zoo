from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import HologramViewSet

router = DefaultRouter()
router.register(r'holograms', HologramViewSet)

urlpatterns = [
    path('', include(router.urls)),
]

