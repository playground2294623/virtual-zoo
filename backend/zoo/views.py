from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from .models import Hologram
from .serializers import HologramSerializer

class HologramViewSet(viewsets.ModelViewSet):
    queryset = Hologram.objects.all()
    serializer_class = HologramSerializer  

    def create(self, request, *args, **kwargs):
        if isinstance(request.data, list):
            serializer = HologramSerializer(data=request.data, many=True)
        else:
            serializer = HologramSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)        
    
    @action(detail=False, methods=['delete'])
    def all(self, request):
        Hologram.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
 