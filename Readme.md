# Virtual Zoo

Start the Virtual-Zoo with:

'docker compose up --build'

## Virtual-Zoo Experience

Head to localhost:4200, feel free to ignore the Landingpage and go to "Holograms". 
Dont Forget to Reset your Dataset for the maximum Virtual-Zoo Experience.

### Features

Choose your language and have a nice chat with yourself in a second Browser-Window.

